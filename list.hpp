///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file list.hpp
/// @version 1.0
///
///
//
/// @author Christopher Agcanas <agcanas8@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   06 Apr 2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <cassert>
#include "node.hpp"

using namespace std;

   class DoubleLinkedList{
      protected:
         Node* head = nullptr;
         Node* tail = nullptr;
         unsigned int count = 0;

      public:
         const bool empty() const;
         void push_front( Node* newNode );
         Node* pop_front();
         Node* get_first() const;
         Node* get_next( const Node* currentNode ) const;
         unsigned int size() const;
         
         //New Functions
         void push_back( Node* newNode );
         Node* pop_back();
         Node* get_last() const;
         Node* get_prev( const Node* currentNode) const;
         
         void insert_after(Node* currentNode, Node* newNode);
         void insert_before(Node* currentNode, Node* newNode);

         void swap(Node* node1, Node* node2);

         const bool isSorted() const;
         void insertionSort();

         //Helper Function
         bool validate() const;
         const bool isIn( Node* aNode) const;
         bool isBefore(Node* a, Node* b);
         bool isAfter(Node* a, Node* b);
   };

