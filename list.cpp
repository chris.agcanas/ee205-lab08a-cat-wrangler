///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 4
///
/// @file list.cpp
/// @version 1.0
///
///
/// @author Christopher Agcanas <agcanas8@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   06 Apr 2021
//////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <cstdlib>
#include <cassert>

#include "list.hpp"
#include "node.hpp"

//return true if list is empty.
const bool DoubleLinkedList::empty() const{ 
   return (head == nullptr);
}

//Return size of List
unsigned int DoubleLinkedList::size() const{
   return count;
}

//////////PUSH FUNCTIONS///////////////////////////

//add newNode to front of the list.
void DoubleLinkedList::push_front( Node* newNode ){
   //If newNode is nothing, return quietly
   if (newNode == nullptr)
      return;

   //If the list is not empty
   if(head != nullptr){
      // assign new node's next to old head
      newNode->next = head;
      newNode->prev = nullptr;
      // old head prev is assigned to newNode
      head->prev = newNode;
      //head assigned to newNode
      head = newNode;
   } else { // If the list is empty
      newNode->next = nullptr;
      newNode->prev = nullptr;
      head = newNode;
      tail = newNode;
   }
   count++;
}//push_front

// Add newNode to the back of the list
void DoubleLinkedList::push_back( Node* newNode){
   // New node is empty
   if(newNode == nullptr)
      return;

   //List is not empty
   if(tail != nullptr){
      tail->next = newNode;
      newNode->prev = tail;
      newNode->next = nullptr;
      tail = newNode;


      /*
      newNode->next = nullptr;
      newNode->prev = tail;
      tail->next = newNode;
      tail = newNode;*/

   }else{ //List is empty
      newNode->next = nullptr;
      newNode->prev = nullptr;
      head = newNode;
      tail = newNode;
   }
   
   count++;
   validate();
   
   return;
}//push_back

//////END OF PUSH FUNCTION////////////////

/////////POP FUNCTION//////////////////////

//remove a node from the front of the list.
Node* DoubleLinkedList::pop_front(){
   //if list is empty return nullptr
   if (head == nullptr)
      return nullptr;
   
   //List is not empty
   Node* temp = head;

   if(head == tail){
      head = nullptr;
      tail = nullptr;
      count--;

      //cout << "Count should be zero. Count is " << count << endl;

      validate();
      return temp;
   }


   head = head -> next;
   head->prev = nullptr;

   count--;
   validate();
   
   return temp;
}//pop_front

//Remove node from back of the list
Node* DoubleLinkedList::pop_back(){
   //if list is empty
   if(head == NULL)
      return nullptr;

   //List is not empty
   Node* temp = tail;

   if (head == tail){
      head = nullptr;
      tail = nullptr;
      
      count--;
      
      return temp;
   }

   tail = tail->prev;
   tail->next = nullptr;
   
   count--;
   validate();
   
   return temp;
}//pop_back

//////////END OF POP FUNCTION//////////////

/////////GET FUNCTIONS//////////////////////

//Return first node in the list
Node* DoubleLinkedList::get_first() const{
   return head;
}

//Return the next node from current node
Node* DoubleLinkedList::get_next( const Node* currentNode) const{
   return currentNode->next;
}

//Return node before currentNode
Node* DoubleLinkedList::get_prev( const Node* currentNode ) const {
   return currentNode->prev;
}

//Return last node
Node* DoubleLinkedList::get_last() const{
   return tail;
}

/////////END OF GET FUNCTION////////////////

/////////////HELPER FUNCETIONS//////////////
//Check if list is still valid (See Lecture Slide)
bool DoubleLinkedList::validate() const{
   if (head == nullptr) {
      assert (tail == nullptr);
      assert (empty());
      assert (size()==0);
   } else {
      assert(tail!= nullptr);
      assert(!empty());
      assert(size()>0);
   }
   if (tail== nullptr) {
      assert (head== nullptr);
      assert (empty());
      assert (size()==0);
   } else {
      assert(head!= nullptr);
      assert(!empty());
      assert(size()>0);
   }
   if (head != nullptr && tail == head) {
//      cout << "Validate(): There should be one item in the list, Count = " << count << endl;
      assert(size()==1);
   }
   
   unsigned int forwardCount = 0;
   Node* currentNode = head;
   while( currentNode !=nullptr){
      forwardCount++;
      if (currentNode->next != nullptr)
         assert( currentNode->next->prev==currentNode);
      currentNode=currentNode->next;
   }

//   cout<<"Validate(): forwardCount = " << forwardCount << " and count = " << count << endl;

   assert( size() == forwardCount);
   
   unsigned int backwardCount = 0;
   currentNode = tail;
   while( currentNode !=nullptr){
      backwardCount++;
      if (currentNode->prev!= nullptr)
         assert( currentNode->prev->next==currentNode);
      currentNode=currentNode->prev;
   }
   assert( size() == backwardCount);

   return true;
}//validate


//Check if node is in the list
const bool DoubleLinkedList::isIn( Node* aNode ) const {
   Node* currentNode = head;

   while( currentNode != nullptr){
      if( aNode == currentNode )
         return true;

      //Move to next node
      currentNode = currentNode->next;
   }

   return false; //Node not found in list
}

//Check if a is before b
bool DoubleLinkedList::isBefore(Node* a, Node* b){
   Node* temp = head;

   while(temp != nullptr){
      //a is before b
      if(a == temp){
         return true;
      }else if(b == temp){
         //b is before a
         return false;
      }
      temp = temp->next;
   }
   return false;
}//isIn

////////////END OF HELPER FUNCTION///////////

/////////////INSERT FUNCETIONS//////////////
//Insert newNode immediately after currentNode
void DoubleLinkedList::insert_after(Node* currentNode, Node* newNode){
   //If the list is empty
   if( currentNode == nullptr && head == nullptr){
      push_front(newNode);
      return;
   }
      
   //Checkings nodes
   if(currentNode != nullptr && head == nullptr){
      //List should be empty but current node says otherwise
      assert(false);
   }

   if(currentNode == nullptr && head != nullptr){
      assert(false);
   }

   assert(currentNode != nullptr && head != nullptr);
   //current node should be in the list
   assert(isIn(currentNode));
   //newNode should not in the list yet
   assert(!isIn(newNode));

   if(tail != currentNode){
      //currentNode not at the end of the list
      newNode->next = currentNode->next;
      currentNode->next = newNode;
      newNode->prev = currentNode;
      newNode->next->prev = newNode;
   
      count++;
   
   }else{// Inserting at the end of the list
      push_back(newNode);
   }

   validate();
   
}//insertAfter

//Insert newNode imediately before currentNode
void DoubleLinkedList::insert_before(Node* currentNode, Node* newNode){
   //If the list is empty
   if( currentNode == nullptr && head == nullptr){
      push_front(newNode);
      return;
   }
      
   //Checkings nodes
   if(currentNode != nullptr && head == nullptr){
      //List should be empty but current node says otherwise
      assert(false);
   }

   if(currentNode == nullptr && head != nullptr){
      assert(false);
   }

   assert(currentNode != nullptr && head != nullptr);
   //current node should be in the list
   assert(isIn(currentNode));
   //newNode should not in the list
   assert(!isIn(newNode));

   if(head != currentNode){
      //Current node not at begining of the list
      newNode->prev = currentNode->prev;
      currentNode->prev->next = newNode;
      currentNode->prev = newNode;
      newNode->next = currentNode;
      
      count++;
      
   }else{
      //Current node at beggin of the list
      push_front( newNode);
   }
      
   validate();

}//insertBefore

////////END OF INSERT FUNCTION////////////


/////////SWAP FUNCTION////////////////////

void DoubleLinkedList::swap(Node* node1, Node* node2){
   //Check if both nodes are in the list
   assert(isIn(node1) && isIn(node2));

   //If node1 and node2 are the same node
   if(node1==node2){
      //do nothing
      return;
   }
   
   //If node1 is not to the left of node2
   if(!isBefore(node1,node2)){
      Node* temp = node1;
      node1 = node2;
      node2 = temp;
   }

   //Storing original values
   Node* n1_left = node1->prev;
   Node* n1_right = node1->next;
   Node* n2_left = node2->prev;
   Node* n2_right = node2->next;

   //if nodes are next to each other
   bool isAdjacent = (n1_right == node2);

   if(!isAdjacent){
   //nodes are not adjacent
   //[1] = [] = [2]
   //node1 points back to what node2 was pointing to
      node1->prev = node2->prev;
   }else{
   //nodes are adjacent
   //[1] = [2] = []
   //node1 points back tot node2
      node1->prev = node2;
   }
   
   if(node1 != head){
   //node1 is not the first in the list
   //[] = [1] = [2]
   //n1_left links to node2
      n1_left->next  = node2;
      node2->prev = n1_left;
   }else{
   //node1 was the head so node2 is now the head
   //[1] = [] = [2]
      node2->prev = nullptr;
      head = node2;
   }

   if(!isAdjacent){
   //nodes are not adjacent
   //[1] = [] = [2]
   //node2 points to what node1 was pointing to 
      node2->next = n1_right;
   }else{
   //nodes are adjacent
   //[1]=[2]=[]
   //node2 links back to node1
      node2->next = node1;
   }

   if(node2 != tail){
   //node2 was not at the end of the list
   //[1] = [] =[2]
      n2_right->prev = node1;
      node1->next = n2_right;
   }else{
   //node2 was the last in the list so tail is now node1
      node1->next = nullptr;
      tail = node1;
   }

   if(!isAdjacent){
   //Fixing links for nodes adjacent to node1 and node2
      n1_right->prev = node2;
      n2_left->next = node1;
   }

  validate(); 

}//swap


/////////////////////END OF SWAP FUNCTION///////////

//////////////////SORT FUNCTION////////////////////
const bool DoubleLinkedList::isSorted() const{
   if(count <= 1)
      return true; //empty list or only one node

   for( Node* i = head; i->next != nullptr; i= i->next) {
      if (*i > *i->next )
         return false;
   }
   return true;
}


void DoubleLinkedList::insertionSort(){
   if(count<=1){
      //List is empty or only has one entry
      return;
   }
  
   //Going through list
   for(Node* i = head->next; i != nullptr; i = i->next){
      //Compare to previous nodes
      for(Node* p = i->prev; p != nullptr; p = p->prev){
         if(*p > *i){
            //Previous entry is greater than current
            swap(p,i);
         }
      }
   }


   validate();
   return;
}//insertionSort

