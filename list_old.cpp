///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 4
///
/// @file list.cpp
/// @version 1.0
///
///
/// @author Christopher Agcanas <agcanas8@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   06 Apr 2021
//////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <cstdlib>
#include <cassert>

#include "list.hpp"
#include "node.hpp"


   //Helper Functions   
   bool DoubleLinkedList::validate() const{
   if( head == nullptr ){
      assert( tail == nullptr );
      assert( count == 0 );
   } else {
      assert( tail != nullptr );
      assert( count != 0 );
   }
   
   if( tail == nullptr ){
      assert( head == nullptr );
      assert( count == 0 );
   } else {
      assert( head != nullptr );
      assert( count != 0 );
   }
   if( (head != nullptr) && (tail == head) ) {
      assert( count == 1 );
   }
   unsigned int forwardCount = 0;
   Node* currentNode = head;
   while( currentNode != nullptr ){
      forwardCount++;
      if(currentNode->next != nullptr ){
         assert( currentNode->next->prev == currentNode );
      }
      currentNode = currentNode->next;
   }
   assert( count == forwardCount );
   unsigned int backwardCount = 0;
   currentNode = tail;
   while( currentNode != nullptr ){
      backwardCount++;
      if(currentNode->prev != nullptr ){
         assert( currentNode->prev->next == currentNode );
      }
      currentNode = currentNode->prev;
   }
   assert( count == backwardCount );

   return true; 
}

 


   const bool DoubleLinkedList::isIn( Node* aNode ) const {
      Node* currentNode = head;

      while( currentNode != nullptr){
         if( aNode == currentNode )
            return true;

         //Move to next node
         currentNode = currentNode->next;
      }

      return false; //Node not found in list
   }


   //return true if list is empty.
   const bool DoubleLinkedList::empty() const{ 
      return (head == nullptr);
   }

   //add newNode to front of the list.
   void DoubleLinkedList::push_front( Node* newNode ){
      //If newNode is nothing, return quietly
      if (newNode == nullptr)
         return;

      //If the list is not empty
      if(head != nullptr){
         // assign new node's next to old head
         newNode->next = head;
         newNode->prev = nullptr;
         // old head prev is assigned to newNode
         head->prev = newNode;
         //head assigned to newNode
         head = newNode;
      } else { // If the list is empty
         newNode->next = nullptr;
         newNode->prev = nullptr;
         head = newNode;
         tail = newNode;
      }
      cout << "*****IN PUSH FRONT****" << endl;
      count++;
      cout << "Count = " << count << endl;
   }

   //remove a node from the front of the list.
   Node* DoubleLinkedList::pop_front(){
      //if list is empty return nullptr
      if (head == nullptr)
         return nullptr;
      //List is not empty

      Node* temp = head;
      
      if(head == tail){
         head = nullptr;
         tail = nullptr;
         count--;
         validate();
         return temp;
      }


      head = head -> next;
      head->prev = nullptr; 
      count--;
      validate();
cout << "*****IN POP FRONT****" << endl;
cout << "Count = " << count << endl;

      return temp;
   }
   
   //Return first node in the list
   Node* DoubleLinkedList::get_first() const{
      return head;
   }
   
   //Return the next node from current node
   Node* DoubleLinkedList::get_next( const Node* currentNode) const{
      return currentNode->next;
   }
   
   //Return size of List
   unsigned int DoubleLinkedList::size() const{
      return count;
   }

//New Functions   
   // Add newNode to the back of the list
   void DoubleLinkedList::push_back( Node* newNode){
      // New node is empty
      if(newNode == nullptr)
         return;
      
      //List is not empty
      if(tail != nullptr){
         newNode->next = nullptr;
         newNode->prev = tail;
         tail->next = newNode;
         tail = newNode;
      }else{ //List is empty
         newNode->next = nullptr;
         newNode->prev = nullptr;
         head = newNode;
         tail = newNode;
      }
      count++;
      validate();
      
cout << "*****IN PUSH FRONT****" << endl;
cout << "Count = " << count << endl;

      return;
   }
   
   //Remove node from back of the list
   Node* DoubleLinkedList::pop_back(){
      //if list is empty
      if(head == NULL)
         return nullptr;
      
      //List is not empty
      Node* temp = tail;
      
      if (head == tail){
         head = nullptr;
         tail = nullptr;
         count--;
         return temp;
      }


      tail = tail->prev;
      tail->next = nullptr;
      count--;
      validate();
   
cout << "*****IN POP BACK****" << endl;
cout << "Count = " << count << endl;

      return temp;
   } 
   //Return last node
   Node* DoubleLinkedList::get_last() const{
      return tail;
   }
   
   //Return node before currentNode
   Node* DoubleLinkedList::get_prev( const Node* currentNode ) const {
      return currentNode->prev;
   }

   //Insert newNode immediately after currentNode
   void DoubleLinkedList::insert_after(Node* currentNode, Node* newNode){
      //If the list is empty
      if( currentNode == nullptr && head == nullptr){
         push_front(newNode);
         return;
      }
      
      //Checkings nodes
      if(currentNode != nullptr && head == nullptr){
         //List should be empty but current node says otherwise
         assert(false);
      }

      if(currentNode == nullptr && head != nullptr){
         assert(false);
      }

      assert(currentNode != nullptr && head != nullptr);
      //current node should be in the list
      assert(isIn(currentNode));
      
      //newNode should not in the list yet
      assert(!isIn(newNode));

      if(tail != currentNode){
      //currentNode not at the end of the list
      newNode->next = currentNode->next;
      currentNode->next = newNode;
      newNode->prev = currentNode;
      newNode->next->prev = newNode;
      count++;
      }else{// Inserting at the end of the list
         push_back(newNode);
      }

      validate();
   }

   void DoubleLinkedList::insert_before(Node* currentNode, Node* newNode){
      //If the list is empty
      if( currentNode == nullptr && head == nullptr){
         push_front(newNode);
         return;
      }
      
      //Checkings nodes
      if(currentNode != nullptr && head == nullptr){
         //List should be empty but current node says otherwise
         assert(false);
      }

      if(currentNode == nullptr && head != nullptr){
         assert(false);
      }

      assert(currentNode != nullptr && head != nullptr);
      //current node should be in the list
      assert(isIn(currentNode));
      
      //newNode should not in the list
      assert(!isIn(newNode));

      if(head != currentNode){
      //Current node not at begining of the list
         newNode->prev = currentNode->prev;
         currentNode->prev->next = newNode;
         currentNode->prev = newNode;
         newNode->next = currentNode;
         count++;
      }else{
      //Current node at beggin of the list
         push_front( newNode);
      }
      
      validate();

   }

   //Node a is before node b
   bool DoubleLinkedList::isBefore(Node* a, Node* b){
      while (a != nullptr){
         if(a->next == b)
            return true;
         a = a->next;
      }

      return false;
   }

   //Node a is after Node b
   bool DoubleLinkedList::isAfter(Node* a, Node* b){
      while (a != nullptr){
         if(a->prev == b)
            return true;
         a = a->prev;
      }

      return false;
   }


   void DoubleLinkedList::swap(Node* node1, Node* node2){
      //Check if both nodes are in the list
      assert(isIn(node1) && isIn(node2));

      //If node1 and node2 are the same node
      if(node1==node2){
         return;
      }


      //Check if both nodes are not empty
      assert(node1 != nullptr && node2 != nullptr);

      Node* a;
      Node* b;
      Node* afterA;
      Node* afterB;
      
      //If node1 is after node2;
      if(isAfter(node1,node2)){
         cout << "Node1 is after Node 2" << endl;
         a = node2;
         b = node1;
      }else{
         cout << "Node2 is after Node1" << endl;
         a = node1;
         b = node2;
      }
     
      
      //Store prev and next before swappingi (old values)
      afterA = a->next;
      afterB = b->next;
     

      //if a is the head of the list
      if(a == head){
         //a takes the place of b
         a->prev = b->prev;
         b->prev->next = a;

         //b is not at the end of the list
         if(b != tail){
            //the node after b points back to a
            b->next->prev = a;
            
            //node after A points back to B
            a->next->prev = b;

            //Swap nextst
            //Node* temp = a->next;
            a->next = afterB;
            b->next = afterA;
         }else{ //Node b is at the end of the list
            b->next = afterA;
            a->next->prev = b;
            a->next = afterA;
            tail = a;
         }
         
         //B is the new head
         b->prev = nullptr;
         head = b;
         cout << "In swap: Count =" << count << endl;
         return;
      }



   }

   const bool DoubleLinkedList::isSorted() const {
      if(count <= 1)
         return true;

      for(Node* i = head; i->next != nullptr; i = i->next){
         if(*i > *i->next)
            return false;
      }

      return true;

   }
